#!/bin/sh -e

XDG_STATE_HOME=${XDG_STATE_HOME:-~/.local/state}
STATE_DIR=$XDG_STATE_HOME/srandr/

get_state() {
    xrandr --verbose | sed -n "
    / disconnected /d
    /^[^[:space:]]/ {
        /connected/ {
            s/([^)]*)//g
            /primary/ ! {
                s/connected/& non_primary/
            }
            s/\([0-9]\+\)x\([0-9]\+\)+\([0-9]\+\)+\([0-9]\+\)/ \3 \4 \1 \2/
            p
        }
    }
    /EDID/,/:/ {

        # Ignore edid line
        /EDID/n
        # collect all the hex bytes
        /:/ ! {
            H
        }
        # Reached end, print all the stored bytes without whitespace
        # and clear the hold space
        /:/ {
            x
            s/[[:space:]]//g
            p
            s/.*//
            x
        }
    }
    "
}

format_state() {
    # eDP-1 connected primary 0 0 1600 900 normal  309mm x 174mm
    # DP-1 connected  non_primary
    while read -r mon _ primary x y width height rotation _; do
        read -r edid
        if [ -z "$x" ]; then
            echo "# $mon not connected"
            continue
        fi
        echo "$edid $mon $primary $x $y ${width} $height $rotation"
    done
}

get_signature() {
    if [ $# -ne 0 ]; then
        printf "# ";
    fi
    for edid in /sys/class/drm/card*-*/edid; do
        xxd -p <"$edid" | tr -d "\n"
        echo
    done | sort | tr -d "\n"
}

save() {
    mkdir -p "$STATE_DIR"
    state_file="$STATE_DIR/$1"
    [ -n "$1" ] || state_file="$STATE_DIR/$(get_signature | md5sum | cut -d" " -f1)"
    {
        get_state | format_state
        printf "# Signature\n"
        get_signature 1
    } > "$state_file"
}

# shellcheck disable=SC2086
load() {
    mkdir -p "$STATE_DIR"
    match_file="$STATE_DIR/$1"
    # shellcheck disable=SC2062
    [ -n "$1" ] || match_file=$(get_signature 1 | grep -x -Fl -f - "$STATE_DIR"/* | head -n1)

    [ -n "$match_file" ] || exit 2
    xrandr -q | grep 'connected' | cut -d ' ' -f 1 | {
        cmd=
        while read -r output; do
            cmd="$cmd --output $output --off"
        done
        xrandr $cmd
    }
    get_state | format_state | while read -r edid mon _; do
        echo "s/$edid [^ ]\+/$edid $mon/"
    done | sed -f - -e "/#/d" "$match_file" | {
        cmd=
        while read -r _ mon primary x y width height rotation _; do
            primary_cmd=
            [ "$primary" != "primary" ] || primary_cmd=--primary
            cmd="$cmd --output $mon $primary_cmd --pos ${x}x${y} --rotate $rotation --mode ${width}x${height}"
        done
        xrandr $cmd
    }
}

if [ $# -eq 0 ]; then
    action=sig
else
    action=$1
    shift
fi
case "$action" in
    sig*)
        get_signature "$@";;
    save)
        save "$@";;
    load)
        load "$@";;
    state)
        get_state | format_state;;
esac
